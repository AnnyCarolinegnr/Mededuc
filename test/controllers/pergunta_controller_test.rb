require 'test_helper'

class PerguntaControllerTest < ActionController::TestCase
  setup do
    @perguntum = pergunta(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pergunta)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create perguntum" do
    assert_difference('Perguntum.count') do
      post :create, perguntum: { justop1: @perguntum.justop1, justop2: @perguntum.justop2, justop3: @perguntum.justop3, nivel: @perguntum.nivel, op1: @perguntum.op1, op2: @perguntum.op2, op3: @perguntum.op3, pergunta: @perguntum.pergunta, reforco: @perguntum.reforco, resp: @perguntum.resp }
    end

    assert_redirected_to perguntum_path(assigns(:perguntum))
  end

  test "should show perguntum" do
    get :show, id: @perguntum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @perguntum
    assert_response :success
  end

  test "should update perguntum" do
    patch :update, id: @perguntum, perguntum: { justop1: @perguntum.justop1, justop2: @perguntum.justop2, justop3: @perguntum.justop3, nivel: @perguntum.nivel, op1: @perguntum.op1, op2: @perguntum.op2, op3: @perguntum.op3, pergunta: @perguntum.pergunta, reforco: @perguntum.reforco, resp: @perguntum.resp }
    assert_redirected_to perguntum_path(assigns(:perguntum))
  end

  test "should destroy perguntum" do
    assert_difference('Perguntum.count', -1) do
      delete :destroy, id: @perguntum
    end

    assert_redirected_to pergunta_path
  end
end
