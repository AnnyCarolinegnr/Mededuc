# MEDEDUC

- Anny Caroline
- Isabelle Barbalho
- Priscila França

## Vídeos
[Playlist do YouTube com os vídeos da aplicação em funcionamento](https://www.youtube.com/playlist?list=PL-Vy8N4zUqm4llHHjyIcU2bghb0YJFyWz "Playlist no YouTube")

## Modelos

### Modelo de Caso de Uso
Diagrama utilizado para determinar as atividades realizadas pelo sistema
![modelo de casos de uso][casosDeUso]

### Modelo de Organização
Descreve a organização entre os agentes, recursos e tarefas. Mostra quais tarefas estão relacionadas e os responsáveis por sua execução.  
Os fluxos de trabalho definem as associações entre as tarefas e informações sobre sua execução.
![modelo de organização][organizacao]

### Modelo de Organização - Workflow
![modelo de organização - workflow][organizacaoWorkFlow]

### Modelo de Agentes
![modelo de agentes][agentes]

### Modelo de Metas e Tarefas
Modelo que representa a decomposição das tarefas descrevendo as consequências de executá-las, quem esta envolvido e quais os recursos necessários.  
Além disso mostra como uma tarefa pode ter dependência com outras, o que a desencadeia no sistema e quais ações a antecedem.  
![modelo de metas e tarefas][metasETarefas]

### Modelo de Interação
Aborda a troca de informações entre os agentes e a aplicação de forma resumida.  
Para especificar detalhadamente como funciona a interação é usado outro modelo/digrama complementar, que pode ser UML ou GRACIA  
Utilizado o modelo GRACIA. Este especifica a sequencia de eventos desencadeados no sistema pela ação do agente.  
![modelo de interação][interacao]

### Modelo GRACIA
![modelo GRACIA][gracia]

### Modelo de Ambiente
Determina as entidades com as quais os sistema multi-agente interage, que são:  
- Recursos: elementos que não exigem API
- Outros agentes
- Aplicativos  
Mostra quais elementos do sistema o agente percebe a execução e começa a atuar e quais componentes do sistema necessitam da atuação direta do agente.  
![modelo de ambiente][ambiente]

## Telas
### Tela inicial
![tela inicial][telaInicial]
### Tela de Login
![tela de login][telaLogin]
### Tela de cadastro de usuários
![cadastro de usuário][cadastroDeUsuario]
### Pergunta com imagem
![pergunta com imagem][perguntaComImagem]
### Resposta certa
![acertou][acertou]
### Resposta errada com justificativa
![justificativa][justificativa]
### Justificativa exibida em um pop up
obs: o pop-up é exibido quando o usuário clica em um dos ícones de informação presentes nas alternativas.  
![justificativaPopUp][justificativaPopUp]
### Reprovado em um nível
![reprovado][reprovado]
### Aprovado em um nível
![aprovado][aprovado]
### Lista de perguntas
obs: visível somente para usuários administradores 
![lista de perguntas (parte 1 de 3)][perguntasPt1]
![lista de perguntas (parte 2 de 3)][perguntasPt2]
![lista de perguntas (parte 3 de 3)][perguntasPt3]
### Edição de perguntas
obs: visível somente para usuários administradores 
![editar uma pergunta (parte 1 de 3)][editarPerguntaPt1]
![editar uma pergunta (parte 2 de 3)][editarPerguntaPt2]
![editar uma pergunta (parte 3 de 3)][editarPerguntaPt3]
### Gameover
![Gameover][gameover]
### Final do jogo com estatísticas
![final do jogo com estatísticas][finalDoJogo]

[casosDeUso]: imgs/modelos/modelo_casos_de_uso.png
[organizacao]: imgs/modelos/modelo_organização.png
[organizacaoWorkFlow]: imgs/modelos/modelo_organização_workflow.png
[agentes]: imgs/modelos/modelo_agente.png
[metasETarefas]: imgs/modelos/modelo_tarefa.png
[interacao]: imgs/modelos/modelo_interacao.png
[gracia]: imgs/modelos/modelo_gracia.png
[ambiente]: imgs/modelos/modelo_ambiente.png

[telaInicial]: imgs/telas/tela-inicial.png
[telaLogin]: imgs/telas/tela-inicial-tela-de-login.png
[cadastroDeUsuario]: imgs/telas/tela-de-cadastro-de-usuario.png
[perguntaComImagem]: imgs/telas/pergunta-com-imagem.png
[acertou]: imgs/telas/acertou.png
[justificativa]: imgs/telas/justificativa.png
[justificativaPopUp]: imgs/telas/justificativa-pop-up.png
[reprovado]: imgs/telas/nao-aprovado.png
[aprovado]: imgs/telas/aprovado.png
[perguntasPt1]: imgs/telas/listagem-perguntas.png
[perguntasPt2]: imgs/telas/listagem-perguntas-pt2.png
[perguntasPt3]: imgs/telas/listagem-perguntas-pt3.png
[editarPerguntaPt1]: imgs/telas/editar-pergunta.png
[editarPerguntaPt2]: imgs/telas/editar-pergunta-pt2.png
[editarPerguntaPt3]: imgs/telas/editar-pergunta-pt3.png
[gameover]: imgs/telas/gameover.png
[finalDoJogo]: imgs/telas/final-do-jogo.png