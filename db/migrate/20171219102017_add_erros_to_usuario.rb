class AddErrosToUsuario < ActiveRecord::Migration
  def change
    add_column :usuarios, :erros, :integer, default: 0
  end
end
