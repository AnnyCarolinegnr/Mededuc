class AddNivelamentoToUsuario < ActiveRecord::Migration
  def change
    add_column :usuarios, :nivelamento, :integer, default: 0
  end
end
