# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171219113833) do

  create_table "pergunta", force: :cascade do |t|
    t.string   "pergunta"
    t.string   "op1"
    t.string   "op2"
    t.string   "op3"
    t.string   "justop1"
    t.string   "justop2"
    t.string   "justop3"
    t.integer  "resp"
    t.integer  "nivel"
    t.boolean  "reforco"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.boolean  "nivelamento"
    t.text     "conteudo"
  end

  create_table "resposta", force: :cascade do |t|
    t.integer  "usuario_id"
    t.integer  "perguntum_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "resp"
  end

  add_index "resposta", ["perguntum_id"], name: "index_resposta_on_perguntum_id"
  add_index "resposta", ["usuario_id"], name: "index_resposta_on_usuario_id"

  create_table "usuarios", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "premios"
    t.text     "nome"
    t.boolean  "admin"
    t.string   "on1"
    t.string   "on2"
    t.string   "on3"
    t.string   "on4"
    t.string   "on5"
    t.string   "on"
    t.string   "or1"
    t.string   "or2"
    t.string   "or3"
    t.string   "or4"
    t.string   "or5"
    t.boolean  "reforco"
    t.integer  "nivelamento",            default: 0
    t.integer  "erros",                  default: 0
    t.boolean  "errossemaforo"
  end

  add_index "usuarios", ["email"], name: "index_usuarios_on_email", unique: true
  add_index "usuarios", ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true

end
