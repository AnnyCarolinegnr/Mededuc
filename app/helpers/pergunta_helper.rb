module PerguntaHelper
    
    def truncate(str)
        @tmp = ""
        if str.length > 100 then
            @tmp = "..."
        end
        
        return str[0..100] + @tmp
    end
    
end
