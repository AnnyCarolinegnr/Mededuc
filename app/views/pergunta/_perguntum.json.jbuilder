json.extract! perguntum, :id, :pergunta, :op1, :op2, :op3, :justop1, :justop2, :justop3, :resp, :nivel, :reforco, :created_at, :updated_at
json.url perguntum_url(perguntum, format: :json)
