class Usuario < ActiveRecord::Base
  

  before_create :default_premios
  def default_premios
     self.premios = -1
     
     self.reforco = false
     
     self.on = "103-104-106-110-111-112-108-109-107-105"
     
     self.on1 = "1-4-5-6-7-8-9-10-11-12"
     self.on2 = "13-14-15-16-17-18-19-20-21-22"
     self.on3 = "23-24-25-26-27-28-29-30-31-32"
     self.on4 = "33-34-35-36-37-38-39-40-41-42"
     self.on5 = "43-44-45-46-47-48-49-50-51-52"
     
     self.or1 = "53-54-55-56-57-58-59-60-61-62"
     self.or2 = "63-64-65-66-67-68-69-70-71-72"
     self.or3 = "73-74-75-76-77-78-79-80-81-82"
     self.or4 = "83-84-85-86-87-88-89-90-91-92"
     self.or5 = "93-94-95-96-97-98-99-100-101-102"
  end
  
  has_many :respostum
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
