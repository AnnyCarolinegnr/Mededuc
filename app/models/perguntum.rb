class Perguntum < ActiveRecord::Base
  has_many :respostum
  
  #https://amitrmohanty.wordpress.com/2014/01/20/how-to-get-current_user-in-model-and-observer-rails/
  def self.current
    Thread.current[:perguntum]
  end
  def self.current=(perguntum)
    Thread.current[:perguntum] = perguntum
  end  
end
