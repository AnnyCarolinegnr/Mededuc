class RespostaController < ApplicationController
  before_action :set_respostum, only: [:show, :edit, :update, :destroy]
  
  # GET /resposta
  # GET /resposta.json
  def index
    @resposta = Respostum.all
  end

  # GET /resposta/1
  # GET /resposta/1.json
  def show
    redirect_to :back
  end

  # GET /new
  # GET /new.json
  def new
  end
  
  def to_b(str)
    str == 'true'
  end

  # GET /resposta/nivel/1/1/1
  def nivel
    @usuarioId = current_usuario.id;
    @ordem = params[:ordem]
    @nivel = params[:nivel]
    
    @reforco = false;
    @nivelamento = false;
     
    if (@nivel == "0") then
      @nivelamento = true
      @lista = current_usuario.on;
    elsif (@nivel == "1") then
      @lista = current_usuario.on1;
    elsif (@nivel == "2") then
      @lista = current_usuario.on2;
    elsif (@nivel == "3") then
      @lista = current_usuario.on3;
    elsif (@nivel == "4") then
      @lista = current_usuario.on4;
    elsif (@nivel == "5") then
      @lista = current_usuario.on5;
    elsif (@nivel == "r1") then
      @lista = current_usuario.or1;
      @reforco = true;
    elsif (@nivel == "r2") then
      @lista = current_usuario.or2;
      @reforco = true;
    elsif (@nivel == "r3") then
      @lista = current_usuario.or3;
      @reforco = true;
    elsif (@nivel == "r4") then
      @lista = current_usuario.or4;
      @reforco = true;
    elsif (@nivel == "r5") then
      @lista = current_usuario.or5;      
      @reforco = true;
    end
    
        
    
    @lista = @lista.split('-');
    @idPergunta = @lista[@ordem.to_i - 1].to_i;
    
    @perguntum = Perguntum.where(id: @idPergunta).first;
    
    #pega, dependendo do que está no bd, se a resposta foi correta ou não.
    #Respostum.where(perguntum_id: @idPergunta, usuario_id: current_usuario.id)
    
    @cores = ["", "btn-default", "btn-default", "btn-default", "btn-default", "btn-default", "btn-default", "btn-default", "btn-default", "btn-default", "btn-default"];
    @questoesRespondidas = 0
    @questoesCertas = 0
    for i in 1..10 do
  
      @tmp = Respostum.joins(:perguntum).where(usuario_id: current_usuario.id, perguntum_id: @lista[i-1])
      
      if (@tmp.count > 0) then
        @tmp = @tmp.first
        if (@tmp.resp == @tmp.perguntum.resp) then
          @cores[i] = "btn-success"
          @questoesRespondidas = @questoesRespondidas + 1
          @questoesCertas = @questoesCertas + 1
        elsif (@tmp.resp == nil) then
          @cores[i] = "btn-default"
        else
          @cores[i] = "btn-danger"
          @questoesRespondidas = @questoesRespondidas + 1
        end
      end
    end
    
    @aprovado = false;
    @finalizou = false;
    @aprovadoReforco = false;
    @gameover = false;
    
    
    @tmp = Respostum.where(usuario_id: current_usuario.id, perguntum_id: @idPergunta)
    if (@tmp.count==0) then
      @novo = true;
      @respostum = Respostum.new()
      @respostum.usuario_id = current_usuario.id
      @respostum.perguntum_id = @idPergunta
      @respostum.save()
    else
      @respostum = @tmp.first()
      if @respostum.resp == nil then
        @novo = true;
      else
        @novo = false;
      end
    end    
    
    if @questoesRespondidas == 10 then
      @finalizou = true;
      
      @quantidadeAcertos = [0,0,0,0,0];
      @quantidadeQuestoes = [0,0,0,0,0];
      if (@nivelamento == true) then
        
        for i in 0..4
          @quantidadeAcertos[i]  = Respostum.joins("INNER JOIN pergunta ON pergunta.id = resposta.perguntum_id WHERE resposta.resp=pergunta.resp AND resposta.usuario_id = #{current_usuario.id} AND pergunta.nivel = #{i+1}  AND pergunta.nivelamento = 't'").count()
          @quantidadeQuestoes[i] = Respostum.joins("INNER JOIN pergunta ON pergunta.id = resposta.perguntum_id WHERE resposta.usuario_id = #{current_usuario.id} AND pergunta.nivel = #{i+1} AND pergunta.nivelamento = 't'").count()
          # ^ Same ^ @quantidadeQuestoes[i] = Respostum.joins(:perguntum).where(usuario_id: current_usuario.id, pergunta: {nivel:i+1, nivelamento:true}).count()
        end
        
        @premios = 0;
        if (current_usuario.premios = -1) then
          #acertou todas as questões do nivel 1 => apto para o nível 2
          if (@quantidadeAcertos[0] == @quantidadeQuestoes[0]) then
            @premios = 1;
            current_usuario.premios = 1;
            current_usuario.nivelamento = 1;
          
            #acertou todas as questões do nivel 2 => apto para o nível 3
            if (@quantidadeAcertos[1] == @quantidadeQuestoes[1]) then
              @premios = 2;
              current_usuario.premios = 2;
              current_usuario.nivelamento = 2;
            
              #acertou todas as questões do nivel 3 => apto para o nível 4
              if (@quantidadeAcertos[2] == @quantidadeQuestoes[2]) then
                @premios = 3;
                current_usuario.premios = 3;
                current_usuario.nivelamento = 3;
              
                #acertou todas as questões do nivel 4 => apto para o nível 5
                if (@quantidadeAcertos[3] == @quantidadeQuestoes[3]) then
                  @premios = 4;
                  current_usuario.premios = 4;
                  current_usuario.nivelamento = 4;
                end
              end
            end
          else
            @premios = 0;
            current_usuario.premios = 0;
          end
          
          if (current_usuario.premios <= 0)
            current_usuario.reforco = false;
          end
        
          current_usuario.save();
        
        end
      else
        if @questoesCertas >= 7 then
            @aprovado = true;
            if (@nivel!="r1" && @nivel!="r2" && @nivel!="r3" && @nivel!="r4" && @nivel!="r5") then
              current_usuario.erros = 0;
              current_usuario.errossemaforo = true;
            end
        else
            @aprovado = false;
            
            if (current_usuario.errossemaforo==true)&&(@nivel!="r1" && @nivel!="r2" && @nivel!="r3" && @nivel!="r4" && @nivel!="r5") then
              current_usuario.erros = current_usuario.erros + 1;
              current_usuario.errossemaforo = false;
              current_usuario.reforco = true;
              Respostum.joins(:perguntum).destroy_all(usuario_id: current_usuario.id, pergunta: {reforco: true});
              
              if (current_usuario.erros == 3) then
                #reinicializa jogo ignorando o nivelamento
                Respostum.joins(:perguntum).destroy_all(usuario_id: current_usuario.id, pergunta: {nivelamento: false});
                current_usuario.erros = 0;
                current_usuario.errossemaforo = true;
                current_usuario.nivelamento = 0;
                current_usuario.premios = 0;
                current_usuario.reforco = false;
                
                @gameover = true;
              end
            end

            current_usuario.save();
        end
        
        if (@nivel=="r1" || @nivel=="r2" || @nivel=="r3" || @nivel=="r4" || @nivel=="r5") then
          
          if (@nivel == "r1") then
            @listaN = current_usuario.on1;
          elsif (@nivel == "r2") then
            @listaN = current_usuario.on2;
          elsif (@nivel == "r3") then
            @listaN = current_usuario.on3;
          elsif (@nivel == "r4") then
            @listaN = current_usuario.on4;
          elsif (@nivel == "r5") then
            @listaN = current_usuario.on5;      
          end      
          
          @listaN = @listaN.split('-'); 
          
          if @questoesCertas >= 7 then
            #limpa questões do nivel correspondente
            for i in 1..10 do
              Respostum.destroy_all(usuario_id: current_usuario.id, perguntum_id: @listaN[i-1].to_i);
            end
            #aprovadoReforco
            current_usuario.reforco = false;
            current_usuario.errossemaforo = true;
            current_usuario.save();
            @aprovadoReforco=true;
          else
            #limpa questões do nivel atual (do reforço atual)
            for i in 1..10 do
              Respostum.destroy_all(usuario_id: current_usuario.id, perguntum_id: @lista[i-1].to_i);
            end
            @aprovadoReforco=false;
          end
        elsif(@nivel.to_i - 1 == current_usuario.premios)then
          
          if @questoesCertas >= 7 then
            current_usuario.premios = current_usuario.premios + 1;
          else
            #limpa reforco
            if (@nivel == "1" && @nivel == "2" && @nivel == "3" && @nivel == "4" && @nivel == "5") then
              if (@nivel == "1") then
                @listaN = current_usuario.or1;
              elsif (@nivel == "2") then
                @listaN = current_usuario.or2;
              elsif (@nivel == "3") then
                @listaN = current_usuario.or3;
              elsif (@nivel == "4") then
                @listaN = current_usuario.or4;
              elsif (@nivel == "5") then
                @listaN = current_usuario.or5;      
              end
              
              @listaN = @listaN.split('-'); 
              for i in 1..10 do
                Respostum.destroy_all(usuario_id: current_usuario.id, perguntum_id: @listaN[i-1].to_i);
              end
              
              #reforco = true
              current_usuario.reforco = true;
              Respostum.joins(:perguntum).destroy_all(usuario_id: current_usuario.id, pergunta: {reforco: true});
            end
            
          end
  
          current_usuario.save()
        end
      end
      
    end

    

  end

  # GET /resposta/1/edit
  def edit
      
  end

  # POST /resposta
  # POST /resposta.json
  def create
    @respostum = Respostum.new(respostum_params)

    respond_to do |format|
      if @respostum.save
        format.html { redirect_to @respostum, notice: 'Respostum was successfully created.' }
        format.json { render :show, status: :created, location: @respostum }
      else
        format.html { render :new }
        format.json { render json: @respostum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /resposta/1
  # PATCH/PUT /resposta/1.json
  def update
    respond_to do |format|
      if @respostum.update(respostum_params)
        format.html { redirect_to @respostum, notice: 'Respostum was successfully updated.' }
        format.json { render :show, status: :ok, location: @respostum }
      else
        format.html { render :edit }
        format.json { render json: @respostum.errors, status: :unprocessable_entity }
      end
    end
  end
  

  # DELETE /resposta/1
  # DELETE /resposta/1.json
  def destroy
    @respostum.destroy
    respond_to do |format|
      format.html { redirect_to resposta_url, notice: 'Respostum was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_respostum
      @respostum = Respostum.find(params[:id])
    end
  
    # Never trust parameters from the scary internet, only allow the white list through.
    def respostum_params
      params.require(:respostum).permit(:resp)
    end
end
