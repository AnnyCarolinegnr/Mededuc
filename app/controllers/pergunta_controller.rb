class PerguntaController < ApplicationController
  before_action :set_perguntum, only: [:show, :edit, :update, :destroy]


  # GET /menu
  def menu
    
    @list_labels = ['Nivelamento', 'Nível 1', 'Nível 2', 'Nível 3', 'Nível 4', 'Nível 5'];
    @list_pontuacao = [
      'nenhuma pergunta respondida neste nível',
      'nenhuma pergunta respondida neste nível',
      'nenhuma pergunta respondida neste nível',
      'nenhuma pergunta respondida neste nível',
      'nenhuma pergunta respondida neste nível',
      'nenhuma pergunta respondida neste nível'
    ];
    
    @acertos = Respostum.joins("INNER JOIN pergunta ON pergunta.id = resposta.perguntum_id WHERE resposta.resp=pergunta.resp AND resposta.usuario_id = #{current_usuario.id}").count();
    @respondidas = Respostum.joins(:perguntum).where(usuario_id: current_usuario.id, pergunta:{reforco: false}).count()
    @media = (@acertos * 100/@respondidas).round(2).to_s + "%";
    
    @tmp = Respostum.joins("INNER JOIN pergunta ON pergunta.id = resposta.perguntum_id WHERE resposta.resp=pergunta.resp AND resposta.usuario_id = #{current_usuario.id} AND pergunta.nivelamento = 't'").count();
    if (@tmp > 0)
      @list_pontuacao[0] = @tmp.to_s + "/10"
    end
    
    for i in 1..5
      @tmp = Respostum.joins("INNER JOIN pergunta ON pergunta.id = resposta.perguntum_id WHERE resposta.resp=pergunta.resp AND resposta.usuario_id = #{current_usuario.id} AND pergunta.nivel = #{i} AND pergunta.nivelamento = 'f' AND pergunta.reforco = 'f'").count();
      if (@tmp > 0)
        @list_pontuacao[i] = @tmp.to_s + "/10"
      end
    end
    
  end

  # GET /pergunta/
  # GET /pergunta.json
  def index
    if (current_usuario.admin != true)
      redirect_to root_path();
    end
    
    @pergunta = Perguntum.all.order(reforco: :desc).order(:nivel)
  end
  
  # GET /pergunta/1/1
  def listar
    if (current_usuario.admin != true)
      redirect_to root_path();
    end
    
    if params[:reforco] == "1" then
      @perguntum = Perguntum.where(nivel: params[:nivel], reforco: true)
    else
      @perguntum = Perguntum.where(nivel: params[:nivel], reforco: false)
    end
  end
  
  #GET /randomize/nivel/
  def randomize
    @nivel = params[:nivel]
    
    if (@nivel == "0" && current_usuario.premios < 0) then
      @lista = current_usuario.on;
      @lista = @lista.split('-');
      
      for i in 1..10 do
        Respostum.destroy_all(usuario_id: current_usuario.id, perguntum_id: @lista[i-1].to_i);
      end
    end

    if (@nivel == "0") then
      @lista = current_usuario.on;
    elsif (@nivel == "1") then
      @lista = current_usuario.on1;
    elsif (@nivel == "2") then
      @lista = current_usuario.on2;
    elsif (@nivel == "3") then
      @lista = current_usuario.on3;
    elsif (@nivel == "4") then
      @lista = current_usuario.on4;
    elsif (@nivel == "5") then
      @lista = current_usuario.on5;
    elsif (@nivel == "r1") then
      @lista = current_usuario.or1;
    elsif (@nivel == "r2") then
      @lista = current_usuario.or2;
    elsif (@nivel == "r3") then
      @lista = current_usuario.or3;
    elsif (@nivel == "r4") then
      @lista = current_usuario.or4;
    elsif (@nivel == "r5") then
      @lista = current_usuario.or5;      
    end
    
    @lista = @lista.split('-');   
    @lista = @lista.shuffle
    
    @str = @lista[0].to_s;
    for i in 2..10 do
      @str = @str +"-"+ @lista[i-1].to_s;
    end
    
    if (@nivel == "0") then
      current_usuario.on = @str;
    elsif (@nivel == "1") then
      current_usuario.on1 = @str;
    elsif (@nivel == "2") then
      current_usuario.on2 = @str;
    elsif (@nivel == "3") then
      current_usuario.on3 = @str;
    elsif (@nivel == "4") then
      current_usuario.on4 = @str;
    elsif (@nivel == "5") then
      current_usuario.on5 = @str;
    elsif (@nivel == "r1") then
      current_usuario.or1 = @str;
    elsif (@nivel == "r2") then
      current_usuario.or2 = @str;
    elsif (@nivel == "r3") then
      current_usuario.or3 = @str;
    elsif (@nivel == "r4") then
      current_usuario.or4 = @str;
    elsif (@nivel == "r5") then
      current_usuario.or5 = @str;
    end
    
    current_usuario.save;
    
    redirect_to nivel_path(@nivel,1);
  end
  
  # GET /pergunta/1
  # GET /pergunta/1.json
  def show
    if (current_usuario.admin != true)
      redirect_to root_path();
    end
  end

  # GET /pergunta/new
  def new
    if (current_usuario.admin != true)
      redirect_to root_path();
    end
    @perguntum = Perguntum.new
  end

  # GET /pergunta/1/edit
  def edit
    if (current_usuario.admin != true)
      redirect_to root_path();
    end
  end

  # POST /pergunta
  # POST /pergunta.json
  def create
    if (current_usuario.admin != true)
      redirect_to root_path();
    end
    @perguntum = Perguntum.new(perguntum_params)

    respond_to do |format|
      if @perguntum.save
        format.html { redirect_to @perguntum, notice: 'A pergunta foi criada com sucesso.' }
        format.json { render :show, status: :created, location: @perguntum }
      else
        format.html { render :new }
        format.json { render json: @perguntum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pergunta/1
  # PATCH/PUT /pergunta/1.json
  def update
    if (current_usuario.admin != true)
      redirect_to root_path();
    end
    respond_to do |format|
      if @perguntum.update(perguntum_params)
        format.html { redirect_to @perguntum, notice: 'A pergunta foi alterada com sucesso.' }
        format.json { render :show, status: :ok, location: @perguntum }
      else
        format.html { render :edit }
        format.json { render json: @perguntum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pergunta/1
  # DELETE /pergunta/1.json
  def destroy
    if (current_usuario.admin != true)
      redirect_to root_path();
    end
    @perguntum.destroy
    respond_to do |format|
      format.html { redirect_to pergunta_url, notice: 'A pergunta foi excluída com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_perguntum
      @perguntum = Perguntum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def perguntum_params
      params.require(:perguntum).permit(:pergunta, :conteudo, :op1, :op2, :op3, :justop1, :justop2, :justop3, :resp)
    end
end


